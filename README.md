# README #
クッキークリッカー日本語パッチ（非公式）
by やふりー　 http://twitter.com/@ya_fury

このソフトウェアは、オルテイル氏作の人気ブラウザゲーム
”cookieclicker”のUIを日本へ翻訳する機能を提供します。

Cookie Clicker（本家）
http://orteil.dashnet.org/cookieclicker/

このソフトウェアは非公式なものであり、
使用には、ゲームデータの破損等のリスクが伴います。
すべて自己責任でご利用ください。
#使い方#

javascriptのコンソールで実行すれば動くはずですが
ブックマークレットで読み込むのが楽です。
簡単に適用できるホスティング版も用意しています。

「ホスティング版日本語パッチの利用方法」
http://futrans.net/projects/ccjp/3373/


#翻訳データについて#
本ソフトウェアの作成には
日本語wikiのデータを利用しました。
編集者の皆様に感謝を申し上げます。

cookie clicker 日本語wiki
https://www55.atwiki.jp/cookieclickerjpn/

#ソフトウェアのライセンスについて#
本ソフトウェアは、本家ゲームのライセンスを継承します。
以下の条項に従ってください。
また、ソフトウェアのバグや、改良のアイディア等
ございましたら、お気軽にお問い合わせorコミット下さい。
---以下、本家ライセンス-----------------------------------------------
If you wish to make a translation or a mod of the game, you are required to follow these rules :
rehosting the game, editing the sourcecode, dealing with bugs, and keeping up with the official version’s updates are your own responsibilities

- the page must have a paypal donate button leading to the same url as the official version’s
- the page must retain all the information in the top-bar (including credits), in addition to a link to the official version
- the page must not include advertisements; you are not allowed to make profits from the page
- translations shouldn’t alter game mechanics unless they are also a mod
- the page must clearly state its nature as an unofficial translation or modification

[引用元]
http://orteil42.tumblr.com/post/61387300247/hey-orteil-ive-been-reading-up-on-your-source




